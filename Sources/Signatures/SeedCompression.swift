//
//  SeedCompression.swift
//  
//
//  Created by Jan Mazurczak on 11/11/2020.
//

import Foundation

public extension UUID {
    var data: Data {
        var result = Data(count: 16)
        result[0] = uuid.0
        result[1] = uuid.1
        result[2] = uuid.2
        result[3] = uuid.3
        result[4] = uuid.4
        result[5] = uuid.5
        result[6] = uuid.6
        result[7] = uuid.7
        result[8] = uuid.8
        result[9] = uuid.9
        result[10] = uuid.10
        result[11] = uuid.11
        result[12] = uuid.12
        result[13] = uuid.13
        result[14] = uuid.14
        result[15] = uuid.15
        return result
    }
}

public extension Data {
    var compressedSeed: String {
        base64EncodedString()
            .replacingOccurrences(of: "+", with: "-")
            .replacingOccurrences(of: "/", with: "_")
            .replacingOccurrences(of: "=", with: "")
    }
}
