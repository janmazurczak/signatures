//
//  CanBeSigned.swift
//  
//
//  Created by Jan Mazurczak on 03/02/2021.
//

import Foundation
import CryptoKit

public protocol CanBeSigned {
    var signableParts: [SignablePart<Self>] { get }
    func signature(with complicator: String) -> String
    func signature(with complicator: Data) -> String
}

public extension CanBeSigned {
    
    func signature(with complicator: String) -> String {
        guard let complicatorData = complicator.data(using: .utf8) else { fatalError("Complicator is shit") }
        return signature(with: complicatorData)
    }
    
    func signature(with complicator: Data) -> String {
        let signableData = signableParts.reduce(Data()) {
            $0 + $1.signableData(self)
        }
        let hash = SHA256.hash(data: signableData + complicator)
        let hashData = Data(hash)
        return hashData.compressedSeed
    }
    
    func include<Value>(_ path: KeyPath<Self, Value>, map: @escaping (Value) -> Data) -> SignablePart<Self> {
        .init(path, map: map)
    }
    
    func include(_ path: KeyPath<Self, Data>) -> SignablePart<Self> {
        .init(path) { $0 }
    }
    
    func include(_ path: KeyPath<Self, String>) -> SignablePart<Self> {
        .init(path) { $0.data(using: .utf8)! }
    }
    
    func include<Value: BinaryInteger>(_ path: KeyPath<Self, Value>) -> SignablePart<Self> {
        .init(path) { String($0, radix: 16, uppercase: false).data(using: .utf8)! }
    }
    
}

public struct SignablePart<Root> {
    
    internal init<Value>(_ path: KeyPath<Root, Value>, map: @escaping (Value) -> Data) {
        signableData = {
            map($0[keyPath: path])
        }
    }
    
    let signableData: (Root) -> Data
    
}
