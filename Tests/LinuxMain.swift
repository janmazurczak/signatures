import XCTest

import SignaturesTests

var tests = [XCTestCaseEntry]()
tests += SignaturesTests.allTests()
XCTMain(tests)
